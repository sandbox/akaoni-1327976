<?php

/**
 * @file
 * Contains the SearchApiNonDrupalDataSourceController class.
 */

/**
 * Data source for all non-Drupal indexes.
 */
class SearchApiNonDrupalDataSourceController extends SearchApiAbstractDataSourceController {
// @todo - Make specific to Index Service?
  public function getIdFieldInfo() {
    return array(
      'key' => 'id',
      'type' => 'string',
    );
  }

// @todo
//   - Support.
//   - Make generic.
  public function loadItems(array $ids) {
    throw new SearchApiException(t('Search non-Drupal indexes module cannot load items.'));
  }

  public function getMetadataWrapper($item = NULL, array $info = array()) {
    $info += $this->getPropertyInfo();
    return entity_metadata_wrapper('entity', $item, $info);
  }

  protected function getPropertyInfo() {
    $type_info = search_api_get_item_type_info($this->type);
    $server = search_api_server_load($type_info['server']);
    $info['property info'] = $server->getNonDrupalProperties($type_info['index']);

    return $info;
  }

}
